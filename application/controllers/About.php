<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class About extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        
        // for load helper
        $this->load->helper('url_helper');
        $this->load->model(['Call_us_model', 'Content_model', 'Footer_model']);
    }

    public function index()
    {
        $data['page'] = 'about_view/index';

        $data['call_us'] = $this->Call_us_model->get_call_us();
        $data['footer'] = $this->Footer_model->get_footer();
        
        $data['about1'] = $this->Content_model->get_content(['page' => 'about1']);
        $data['about2'] = $this->Content_model->get_content(['page' => 'about2']);
        $data['about3'] = $this->Content_model->get_content(['page' => 'about3']);

        $this->load->view('app', $data);
    }
}

