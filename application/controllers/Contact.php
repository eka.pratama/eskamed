<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Contact extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        
        // for load helper
        $this->load->helper('url_helper');

        $this->load->helper('form');
        $this->load->library('form_validation');

        $this->load->model(['Contact_info_model', 'Contact_message_model', 'Footer_model']);
    }

    public function index()
    {
        $data['page'] = 'contact_view/index';

        $data['footer'] = $this->Footer_model->get_footer();
        $data['contact_info'] = $this->Contact_info_model->get_contact_info();
        
        $this->load->view('app', $data);
    }

    public function store()
    {
        $this->form_validation->set_rules('name', 'name', 'required');
        $this->form_validation->set_rules('subject', 'subject', 'required');
        $this->form_validation->set_rules('email', 'email', 'trim|required|valid_email');
        $this->form_validation->set_rules('message', 'message', 'required');

        if ($this->form_validation->run() == FALSE){
            $data['page'] = 'contact_view/index';

            $data['footer'] = $this->Footer_model->get_footer();
            $data['contact_info'] = $this->Contact_info_model->get_contact_info();

            $this->load->view('app', $data);
        } else {
 
            $data = [
                'name' => $this->input->post('name'),
                'subject' => $this->input->post('subject'),
                'email' => $this->input->post('email'),
                'message' => $this->input->post('message'),
            ];
            
            $this->Contact_message_model->set_contact_message($data);

            $this->session->set_flashdata('success', 'send message successfully');

            redirect(base_url("contact"));
        }
    }

}

