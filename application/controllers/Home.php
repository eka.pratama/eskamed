<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        
        // for load helper
        $this->load->helper('url_helper');
        $this->load->model(['Call_us_model', 'Content_model', 'Home_banner_model', 'Footer_model']);
    }

    public function index()
    {
        $data['page'] = 'home_view/index';

        $data['call_us'] = $this->Call_us_model->get_call_us();
        $data['home_banner'] = $this->Home_banner_model->get_ajax_list_home_banner();
        $data['footer'] = $this->Footer_model->get_footer();

        $data['home1'] = $this->Content_model->get_content(['page' => 'home1']);
        
        $this->load->view('app', $data);
    }

}

