<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Product extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        
        // for load helper
        $this->load->helper('url_helper');
    }

    public function index()
    {
        $data['page'] = 'product_view/index';
        
        $this->load->view('app', $data);
    }
}

