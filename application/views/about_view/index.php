<!-- start banner Area -->
<section class="relative" id="about" style="background: url('<?php echo base_url()?>/img/about-us/header-about-us.jpg') center;
   background-size: cover;">
   <div class="overlay overlay-bg"></div>
   <div class="container">
      <div class="row d-flex align-items-center justify-content-center">
         <div class="about-content col-lg-12">
            <h1 class="text-white">
               About Us            
            </h1>
            <p class="text-white link-nav"><a href="<?php echo base_url()?>">Home </a> <span class="lnr lnr-arrow-right"></span> <a href="<?php echo base_url()?>product"> About Us</a></p>
         </div>
      </div>
   </div>
</section>
<!-- End banner Area -->
<!-- Start home-about Area -->
<section class="home-about-area section-gap aboutus-about" id="about">
   <div class="container">
      <div class="row justify-content-center align-items-center">
         <div class="col-lg-8 col-md-12 home-about-left">
            <h6><?php echo $about1->title; ?></h6>
            <h1>
               <?php echo $about1->title_description; ?>
            </h1>
            <p class="sub"><?php echo $about1->description; ?></p>
         </div>
         <div class="col-lg-4 col-md-12 relative home-about-right" style="background: linear-gradient(rgba(0, 0, 0, 0.8), rgba(0, 0, 0, 0.8)); background: url('<?php echo base_url()?>uploads/content/<?php echo $about1->image; ?>'); background-size: cover; height: 468px; width: 360px;">
         </div>
      </div>
   </div>
</section>
<!-- End home-about Area -->
</br>
</br>
</br>
<!-- Start cat Area -->
<section class="cat-area section-gap aboutus-cat" id="feature">
   <div class="container">
      <div class="row justify-content-center">
         <div class="col-md-12 text-center">
            <h1 class="font-weight-bold text-black"><?php echo $about2->title; ?></h1>
            </br>
            <p class="mb-5"><?php echo $about2->description; ?></p>
         </div>
      </div>
      <div class="row">
         <?php foreach($about2->content_detail as $about2_content_detail) { ?>
            <div class="col-lg-6">
               <div class="single-cat d-flex flex-column">
                  <a class="hb-sm-margin mx-auto d-block">
                     <span class="hb hb-sm inv hb-facebook-inv">
                        <span class="<?php echo $about2_content_detail->icon; ?>"></span>
                     </span>
                  </a>
                  <h4 class="mb-20" style="margin-top: 23px;"><?php echo $about2_content_detail->title; ?></h4>
                  <p>
                  <?php echo $about2_content_detail->description; ?>
                  </p>
               </div>
            </div>
         <?php } ?>
      </div>
   </div>
</section>
<!-- End cat Area -->
</br>
</br>
</br>
<!-- Hotline Area Starts -->
<div class="site-section" id="faq-section">
   <div class="container">
      <div class="row">
         <div class="col-lg-6 mb-5 mb-lg-0">
            <img src="<?php echo base_url(); ?>uploads/content/<?php echo $about3->image; ?>" alt="Image" class="img-fluid">
         </div>
         <div class="col-lg-6 ml-auto pl-lg-5">
            <span class="sub-title"><?php echo $about3->title; ?></span>
            <h2 class="font-weight-bold text-black mb-5"><?php echo $about3->title_description; ?></h2>
            <div class="accordion" id="accordionExample">
               <div class="accordion-item">
                  <h2 class="mb-0 rounded mb-2">
                     <a href="#" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                     Product Quality</a>
                  </h2>
                  <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                     <div class="accordion-body">
                        <p>We do continuous improvement to our products; not only product quality, but also product variant. We learn to improve and exceed the client's needs.</p>
                     </div>
                  </div>
               </div>
               <div class="accordion-item">
                  <h2 class="mb-0 rounded mb-2">
                     <a href="#" class="collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                     Right People 
                     </a>
                  </h2>
                  <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                     <div class="accordion-body">
                        <p>Our team consists of trained staff with marketing, operational, and financial expertise to ensure the best buying experience and ability to help the client's needs.</p>
                     </div>
                  </div>
               </div>
               <div class="accordion-item">
                  <h2 class="mb-0 rounded mb-2">
                     <a href="#" class="collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                     Quality Policy 
                     </a>
                  </h2>
                  <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                     <div class="accordion-body">
                        <p>Our quality policy was created to serve customer's needs, provided by high-quality medical products.</p>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>