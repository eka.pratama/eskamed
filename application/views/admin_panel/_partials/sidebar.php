<div class="left-sidebar" style="background: #222d32">
    <!-- Sidebar scroll-->
    <div class="scroll-sidebar">
        <!-- Sidebar navigation-->
        <nav class="sidebar-nav" style="background: #222d32">
            <ul id="sidebarnav">
                <li class="nav-devider"></li>

                <li class="nav-label">Home</li>

                <li> 
                    <a class="has-arrow" href="#" aria-expanded="false"><i class="fa fa-home"></i><span class="hide-menu">Home</span></a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="<?php echo base_url(); ?>admin_panel/home_banner">Home Banner</a></li>
                        <li><a href="<?php echo base_url(); ?>admin_panel/content">Content</a></li>
                        <li><a href="<?php echo base_url(); ?>admin_panel/content_detail">Content Detail</a></li>
                        <li><a href="<?php echo base_url(); ?>admin_panel/call_us">Call Us</a></li>
                    </ul>
                </li>

                <li> 
                    <a class="has-arrow" href="#" aria-expanded="false"><i class="fa fa-circle"></i><span class="hide-menu">Product</span></a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="<?php echo base_url(); ?>admin_panel/product_category">Product Category</a></li>
                    </ul>
                </li>

                <li> 
                    <a class="has-arrow" href="#" aria-expanded="false"><i class="fa fa-phone"></i><span class="hide-menu">Contact</span></a>
                    <ul aria-expanded="false" class="collapse">
                        <!-- <li><a href="<?php echo base_url(); ?>contact_title">Contact Title</a></li> -->
                        <li><a href="<?php echo base_url(); ?>admin_panel/contact_message">Contact Message</a></li>
                        <li><a href="<?php echo base_url(); ?>admin_panel/contact_info">Contact Info</a></li>
                        <li><a href="<?php echo base_url(); ?>admin_panel/footer">Footer</a></li>
                        <!-- <li><a href="<?php echo base_url(); ?>contact_info">Contact Info</a></li> -->
                    </ul>
                </li>

                <!-- <li>
                    <a href="mailto:ekapratama363@gmail.com"><i class="fa fa-envelope"></i>
                    <span>Send Error Report</span></a>
                </li> -->
                <!-- END -->
            </ul>
        </nav>
        <!-- End Sidebar navigation -->
    </div>
    <!-- End Sidebar scroll-->
</div>
<!-- End Left Sidebar  -->   