<?php 

    // start header
    $this->load->view('templates/header'); 
    // end header

    // start content
    $this->load->view($page);
    // end content

    // start call us
    isset($call_us) ? $this->load->view('page/call-us') : '';
    // end call us

    // start footer
    $this->load->view('templates/footer'); 
    // end footer
