<!-- Mobile Specific Meta -->
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<!-- Favicon-->
<link rel="icon" type="image/icon" href="<?php echo base_url();?>img/icon/favicon.ico">
<!-- Author Meta -->
<meta name="author" content="codepixer">
<!-- Meta Description -->
<meta name="description" content="">
<!-- Meta Keyword -->
<meta name="keywords" content="">
<!-- meta character set -->
<meta charset="UTF-8">

<link href="https://fonts.googleapis.com/css?family=Poppins:100,200,400,300,500,600,700" rel="stylesheet"> 

<link rel="stylesheet" href="<?php echo base_url();?>fonts/icomoon/style.css">
<!--
CSS
============================================= -->
<link rel="stylesheet" href="<?php echo base_url();?>css/custom-bs.css">
<link rel="stylesheet" href="<?php echo base_url();?>css/linearicons.css">
<link rel="stylesheet" href="<?php echo base_url();?>css/font-awesome.min.css">
<link rel="stylesheet" href="<?php echo base_url();?>css/bootstrap.css">
<link rel="stylesheet" href="<?php echo base_url();?>css/nice-select.css">	
<link rel="stylesheet" href="<?php echo base_url();?>css/hexagons.min.css">							
<link rel="stylesheet" href="<?php echo base_url();?>css/animate.min.css">
<link rel="stylesheet" href="<?php echo base_url();?>css/owl.carousel.css">
<link rel="stylesheet" href="<?php echo base_url();?>css/main.css">

<link href="<?php echo base_url();?>css/responsive.css" rel="stylesheet">

<!-- Icon css link -->
<link href="<?php echo base_url();?>css/icofont.css" rel="stylesheet">

<!-- Bootstrap -->
<link href="<?php echo base_url();?>css/bootstrap.min.css" rel="stylesheet">

<!-- Rev slider css -->
<link href="<?php echo base_url();?>vendors/revolution/css/settings.css" rel="stylesheet">
<link href="<?php echo base_url();?>vendors/revolution/css/layers.css" rel="stylesheet">
<link href="<?php echo base_url();?>vendors/revolution/css/navigation.css" rel="stylesheet">
<link href="<?php echo base_url();?>vendors/animate-css/animate.css" rel="stylesheet">

<!-- Extra plugin css -->
<link href="<?php echo base_url();?>vendors/magnific-popup/magnific-popup.css" rel="stylesheet">
<link href="<?php echo base_url();?>vendors/owl-carousel/assets/owl.carousel.min.css" rel="stylesheet">

<!--<link rel="stylesheet" href="<?php echo base_url();?>css/owl.theme.default.min.css">-->
<link rel="stylesheet" href="<?php echo base_url();?>css/aos.css">
<link rel="stylesheet" href="<?php echo base_url();?>css/ionicons.min.css">
<link rel="stylesheet" href="<?php echo base_url();?>css/bootstrap-datepicker.css">
<link rel="stylesheet" href="<?php echo base_url();?>css/jquery.timepicker.css">
<link rel="stylesheet" href="<?php echo base_url();?>css/flaticon.css">
<link rel="stylesheet" href="<?php echo base_url();?>css/icomoon.css">


<link rel="stylesheet" href="<?php echo base_url();?>css/vendors/linericon/style.css">
<link rel="stylesheet" href="<?php echo base_url();?>css/vendors/owl-carousel/owl.carousel.min.css">
<link rel="stylesheet" href="<?php echo base_url();?>css/vendors/lightbox/simpleLightbox.css">
<link rel="stylesheet" href="<?php echo base_url();?>css/vendors/nice-select/css/nice-select.css">
<link rel="stylesheet" href="<?php echo base_url();?>css/vendors/animate-css/animate.css">
<link rel="stylesheet" href="<?php echo base_url();?>css/vendors/flaticon/flaticon.css">
<link rel="stylesheet" href="<?php echo base_url();?>css/vendors/jquery.fancybox.min.css">

