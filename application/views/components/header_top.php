<div class="header-top">
  <div class="container">
    <div class="row align-items-center">
      <a href="#" class="mx-left d-inline-block d-lg-none site-menu-toggle js-menu-toggle text-black"><span
            class="icon-menu h1" style="padding-left: 15px;"></span>
      </a>
      <div class="col-12 text-center">
        <a href="<?= base_url();?>" class="site-logo">

          <?php foreach($t_logo as $logo) : ?>
          <img src="<?php echo base_url();?>images/logo/<?= $logo->photo ;?>" alt="Image" class="img-fluid">
          <?php endforeach; ?>
          
        </a>
      </div>
    </div>
  </div>
</div>