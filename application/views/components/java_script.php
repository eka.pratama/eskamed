<script src="<?php echo base_url();?>js/vendor/jquery-2.2.4.min.js"></script>

<script src="<?php echo base_url();?>js/jquery.fancybox.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="js/vendor/bootstrap.min.js"></script>			
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBhOdIF3Y9382fqJYt5I_sswSrEw5eihAA"></script>
<script src="<?php echo base_url();?>js/easing.min.js"></script>			
<script src="<?php echo base_url();?>js/hoverIntent.js"></script>
<script src="<?php echo base_url();?>js/superfish.min.js"></script>	
<script src="<?php echo base_url();?>js/jquery.ajaxchimp.min.js"></script>
<script src="<?php echo base_url();?>js/jquery.magnific-popup.min.js"></script>	
<script src="<?php echo base_url();?>js/owl.carousel.min.js"></script>	
<script src="<?php echo base_url();?>js/hexagons.min.js"></script>							
<script src="<?php echo base_url();?>js/jquery.nice-select.min.js"></script>	
<script src="<?php echo base_url();?>js/jquery.counterup.min.js"></script>
<script src="<?php echo base_url();?>js/waypoints.min.js"></script>							
<script src="<?php echo base_url();?>js/mail-script.js"></script>	
<script src="<?php echo base_url();?>js/main.js"></script>
<script src="<?php echo base_url();?>js/background-img.js"></script>

