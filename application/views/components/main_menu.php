<?php foreach($t_web_color1 as $color1) : ?>

<div class="site-navbar py-2 js-sticky-header site-navbar-target d-none pl-0 d-lg-block" role="banner" style="background-color:<?= $color1->color_code;?>;">
  <div class="container">
    <div class="d-flex align-items-center">
      <div class="mx-auto" >
        <nav class="site-navigation position-relative text-center" role="navigation">
          <ul class="site-menu main-menu js-clone-nav mx-auto d-none pl-0 d-lg-block border-none">
            <li><a href="<?php echo base_url()?>about" class="nav-link text-center" style="color:white;">ABOUT US</a></li>
            <li><a href="<?php echo base_url()?>work" class="nav-link text-center" style="color:white;">LET'S WORK TOGETHER</a></li>
            <li><a href="<?php echo base_url()?>blog" class="nav-link text-center" style="color:white;">THE BLOG</a></li>
            <li><a href="<?php echo base_url()?>contact" class="nav-link text-center" style="color:white;">MAKE CONTACT</a></li>
          </ul>                                                                                                               
        </nav>
      </div>
    </div>
  </div>
</div>

<?php endforeach; ?>