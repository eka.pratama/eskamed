<header id="header" id="home">
    <div class="header-top">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-sm-6 col-4 header-top-left no-padding">

                </div>
                <div class="col-lg-6 col-sm-6 col-8 header-top-right no-padding">
                    <a href="tel:021 877 833 896">021 877 833 896</a>
                    <a href="mailto:support@colorlib.com">info@skmgroup.co.id</a>
                </div>
            </div>
        </div>
    </div>
    <div class="container main-menu">
        <div class="row align-items-center justify-content-between d-flex">
            <div id="logo">
                <a href="<?= base_url();?>"><img src="<?php echo base_url();?>img/icon/Logo Eskamed.png" alt="" title="" style="width:120px;" /></a>
            </div>
            <nav id="nav-menu-container">
                <ul class="nav-menu">
                    <li class="menu-active"><a href="<?= base_url();?>">Home</a></li>
                    <li><a href="<?= base_url();?>about">About</a></li>
                    <li class="menu-has-children"><a href="#">Product</a>
                        <ul>
                            <li><a href="<?= base_url();?>product/infusion_therapy">Infusion Therapy</a></li>
                            <li><a href="<?= base_url();?>">Urology</a></li>
                            <li><a href="<?= base_url();?>">Anaesthesia</a></li>
                            <li><a href="<?= base_url();?>">Surgery & Orthopedic</a></li>
                            <li><a href="<?= base_url();?>">Other</a></li>
                        </ul>
                    </li>
                    <li><a href="<?= base_url();?>contact">Contact</a></li>
                </ul>
            </nav>
            <!-- #nav-menu-container -->
        </div>
    </div>
</header>
<!-- #header -->