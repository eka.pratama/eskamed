<!-- start banner Area -->
<section class="relative" id="service" style="background: url('<?php echo base_url()?>/img/header-bg-service.jpg') center;
  background-size: cover;">	
	<div class="overlay overlay-bg"></div>
	<div class="container">				
		<div class="row d-flex align-items-center justify-content-center">
			<div class="about-content col-lg-12">
				<h1 class="text-white">
					Contact Us			
				</h1>	
				<p class="text-white link-nav"><a href="<?php echo base_url()?>">Home </a>  <span class="lnr lnr-arrow-right"></span>  <a href="<?php echo base_url()?>contact"> Contact Us</a></p>
			</div>	
		</div>
	</div>
</section>
<!-- End banner Area -->

<!-- Start contact-page Area -->
<section class="contact-page-area section-gap">
	<div class="container">
		<div class="row">
			<!--<div class="map-wrap" style="width:100%; height: 445px;" id="map"></div>-->
			<div class="col-lg-4 d-flex flex-column address-wrap">
			</br></br></br>
				<?php foreach($contact_info as $data_contact_info) { ?>
					<div class="single-contact-address d-flex flex-row">
						<div class="icon">
							<span class="<?php echo $data_contact_info->icon; ?>"></span>
						</div>
						<div class="contact-details">
							<h5><?php echo $data_contact_info->title; ?></h5>
							<p><?php echo $data_contact_info->description; ?></p>
						</div>
					</div>												
				<?php } ?>		
			</div>
			<div class="col-lg-8">
			</br></br></br>
            
				<?php if(validation_errors()) { ?>
					<div class="alert alert-warning alert-dismissible">
						<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
						<p class="text-white"><?php echo validation_errors(); ?></p>
					</div>
				<?php } ?>

				<?php if($this->session->flashdata('success') != NULL) { ?>
					<div class="alert alert-success alert-dismissible">
						<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
						<p class="text-black"><?php echo $this->session->flashdata('success') ?></p>
					</div>
				<?php } ?>

				<?php echo form_open('contact/store', ['class' => 'contact-form text-right']); ?>

					<div class="row">	
						<div class="col-lg-6 form-group">
							<input type="text" name="name" class="common-input mb-20 form-control" value="<?php echo set_value('name'); ?>" placeholder="Name">
							
							<input type="text" name="email" class="common-input mb-20 form-control" value="<?php echo set_value('email'); ?>" placeholder="Email">
				
							<input type="text" name="subject" class="common-input mb-20 form-control" value="<?php echo set_value('subject'); ?>" placeholder="Subject">
                
							<div class="mt-20 alert-msg" style="text-align: left;"></div>
						</div>
						<div class="col-lg-6 form-group">
							<textarea class="common-textarea form-control" name="message" placeholder="Messege" ><?php echo set_value('message'); ?></textarea>
				
							<input type="submit" class="genric-btn primary circle mt-30" style="float: right; background-color:#22346c;" value="Send Message">
                		</div>
					</div>
            
				<?php echo form_close(); ?>
			
			</div>
		</div>
	</div>	
</section>
			<!-- End contact-page Area -->