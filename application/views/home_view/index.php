<section class="banner-area active-blog-slider">
    <?php foreach($home_banner as $data_home_banner) { ?>
        <div class="item-slider relative" style="background: url(<?php echo base_url(); ?>uploads/home_banner/<?php echo $data_home_banner->image; ?>);background-size: cover;">
            <div class="overlay" style="background: rgba(0,0,0,.3)"></div>
            <div class="container">
                <div class="row fullscreen justify-content-center align-items-center">
                    <div class="col-md-12 col-12">
                        <div class="banner-content text-center">
                            <h4 class="text-white mb-20 text-uppercase"><?php echo $data_home_banner->title; ?></h4>
                            <h1 class="text-uppercase text-white"><?php echo $data_home_banner->title_description; ?></h1>
                            <p class="text-white"><?php echo $data_home_banner->description; ?></p>
                            <a href="#" class="primary-btn header-btn2 text-uppercase">Get Started</a>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    <?php } ?>
</section>

<!--================App Feature Area =================-->
<section class="app_feature_area" id="feature">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12 text-center">
                <h1 class="font-weight-bold text-black">We Started with ESKAMED</h1></br>
                <p class="mb-5">Meet our first brand ESKAMED, high-quality medical disposable products.</p>
            </div>
        </div>
        <div class="row app_feature_inner">
            <div class="col-md-3">
                <div class="app_feature_item_inner">
                    <div class="app_feature_item">
                        <div class="round_icon">
                            <i><img src="<?php echo base_url();?>img/icon/feature1.png" alt="" style="width: 30px; margin-top: -5px;"></i>
                        </div>
                        <a href="#"><h4>High-Quality</h4></a>
                        <p>With ESKAMED - We are committed to ensure our product is high-quality.</p>
                    </div>
                    <div class="app_feature_item">
                        <div class="round_icon">
                            <i><img src="<?php echo base_url();?>img/icon/feature2.png" alt="" style="width: 30px; margin-top: -5px;"></i>
                        </div>
                        <a href="#"><h4>Excellent Services</h4></a>
                        <p>With ESKAMED - We deliver excellent services and products to make a convenient buying experience.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="app_round_image">
                    <img class="round_c" src="img/round-circle.png" alt="">
                    <img class="app_mobile" src="<?php echo base_url();?>img/service.png" alt="">
                </div>
                <div class="app_feature_item app_middle">
                    <div class="round_icon">
                        <i><img src="<?php echo base_url();?>img/icon/feature3.png" alt="" style="width: 30px; margin-top: -5px;"></i>
                    </div>
                    <a href="#"><h4>Continuous Improvements</h4></a>
                    <p>With ESKAMED - We strive to make continuous improvements for our product to fill the client’s needs.</p>
                </div>
            </div>
            <div class="col-md-3">
                <div class="app_feature_item_inner left_feature">
                    <div class="app_feature_item">
                        <div class="round_icon">
                            <i><img src="<?php echo base_url();?>img/icon/feature4.png" alt="" style="width: 30px; margin-top: -5px;"></i>
                        </div>
                        <a href="#"><h4>Trained Staff</h4></a>
                        <p>With ESKAMED - We serve you with our trained staff and make sure their capability can help the client’s needs.</p>
                    </div>
                    <div class="app_feature_item">
                        <div class="round_icon">
                            <i><img src="<?php echo base_url();?>img/icon/feature2.png" alt="" style="width: 30px; margin-top: -5px;"></i>
                        </div>
                        <a href="#"><h4>Excellent Products</h4></a>
                        <p>With ESKAMED - We deliver excellent services and products to make a convenient buying experience.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--================End App Feature Area =================-->

<!-- Start about Area -->
<section class="section-gap info-area" id="about">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12 text-center">
                <h1 class="font-weight-bold text-black">About Us</h1></br>
                <p class="mb-5">High Quality Medical Disposable Products with Excellent Services</p>
            </div>
        </div>
        <div class="single-info row mt-40">
            <div class="col-lg-6 col-md-12 mt-120 text-center no-padding info-left">
                <div class="info-thumb">
                    <img src="<?php echo base_url();?>uploads/content/<?php echo $home1->image; ?>" class="img-fluid" alt="">
                </div>
            </div>
            <div class="col-lg-6 col-md-12 no-padding info-rigth">
                <div class="info-content">
                    <h2 class="pb-30"><?php echo $home1->title; ?></h2>

                    </br>
                    <p><font style="font-weight: bold;"><?php echo $home1->title_description; ?></font></p>
                    <p><?php echo $home1->description; ?> </p>

                    <a href="<?php echo base_url();?>about" class="template-btn mt-3">Read More</a>
                    </br>

                </div>
            </div>
        </div>
    </div>
</section>
<!-- End about Area -->


<div class="site-section" id="our-team-section">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12 text-center">
                <h1 class="font-weight-bold text-black">Our Product</h1></br>
                <p class="mb-5">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eligendi, explicabo, quasi. Magni deserunt sunt labore.</p>
            </div>
        </div>

        <div class="row">

            <div class="col-lg-3 col-md-6 mb-3">
                <div class="person">
                    <div class="bio-img">
                        <figure>
                            <img src="img/product/product-1.png" alt="Image" class="img-fluid">
                        </figure>
                        <div class="social">
                            <a href="<?= base_url();?>product/infusion_therapy" target="_blank">View Product</a>
                        </div>
                    </div>
                    <h2>Infusion Therapy</h2>
                    </br>
                    <p>This treatment method has traditionally been used only in hospitals, but now infusion therapy can be administered in outpatient infusion therapy centers, or even in your home by specially trained nurses. </p>
                </div>
            </div>

            <div class="col-lg-3 col-md-6 mb-3">
                <div class="person">
                    <div class="bio-img">
                        <figure>
                            <img src="img/product/product-2.png" alt="Image" class="img-fluid">
                        </figure>
                        <div class="social">
                             <a href="" target="_blank">View Product</a>
                        </div>
                    </div>
                    <h2>Urology</h2>
                    </br>
                    <p>Urology is a part of health care that deals with diseases of the male and female urinary tract (kidneys, ureters, bladder and urethra).</p>

                </div>
            </div>

            <div class="col-lg-3 col-md-6 mb-3">
                <div class="person">
                    <div class="bio-img">
                        <figure>
                            <img src="img/product/product-3.png" alt="Image" class="img-fluid">
                        </figure>
                        <div class="social">
                             <a href="" target="_blank">View Product</a>
                        </div>
                    </div>
                    <h2>Anaesthesia</h2>
                    </br>
                    <p>Anaesthetics are used during tests and surgical operations to numb sensation in certain areas of the body or induce sleep.</p>
                </div>
            </div>

            <div class="col-lg-3 col-md-6 mb-3">
                <div class="person">
                    <div class="bio-img">
                        <figure>
                            <img src="img/product/product-4.png" alt="Image" class="img-fluid">
                        </figure>
                        <div class="social">
                             <a href="" target="_blank">View Product</a>
                        </div>
                    </div>
                    <h2>Surgery & Orthopedic</h2>
                    </br>
                    <p>Orthopaedic surgeons are devoted to the prevention, diagnosis, and treatment of disorders of the bones, joints, ligaments, tendons and muscles.</p>
                </div>
            </div>

        </div>
    </div>
</div>