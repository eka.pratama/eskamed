<section class="hotline-area text-center section-padding2" style="background: url(img/hotline-bg-2.jpg);background-size: cover;">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h2><?php echo $call_us->title; ?></h2></br>
                <span><?php echo $call_us->title_description; ?></span>
                <p class="pt-3"><?php echo $call_us->description; ?></p>
            </div>
        </div>
    </div>
</section>