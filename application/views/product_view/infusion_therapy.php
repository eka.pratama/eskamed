<!-- start banner Area -->
<section class="relative" id="about" style="background: url('<?php echo base_url()?>/img/header-bg-product.jpg') center;
  background-size: cover;">	
	<div class="overlay overlay-bg"></div>
	<div class="container">				
		<div class="row d-flex align-items-center justify-content-center">
			<div class="about-content col-lg-12">
				<h1 class="text-white">
					Our Product				
				</h1>	
				<p class="text-white link-nav"><a href="<?php echo base_url()?>">Home </a>  <span class="lnr lnr-arrow-right"></span>  <a href="<?php echo base_url()?>product"> Infusion Therapy</a></p>
			</div>	
		</div>
	</div>
</section>
<!-- End banner Area -->

<!--================Projects Area =================-->
<div class="site-section" id="our-team-section">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12 text-center">
                <h1 class="font-weight-bold text-black">Infusion Therapy</h1>
                <p class="mb-5">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eligendi, explicabo, quasi. Magni deserunt sunt labore.</p>
            </div>
        </div>

        <div class="projects_inner row">
            <div class="col-lg-4 col-sm-6">
                <div class="projects_item">
                    <div class="item web">
                        <a href="<?php echo base_url()?>img/product/infusion_therapy/img_1.jpeg" class="item-wrap" data-fancybox="Infusion Therapy">
                            <span style="padding:10px;"> Micro Drop Infusion Set </span>
                            <img class="img-fluid" src="<?php echo base_url()?>img/product/infusion_therapy/img_1.jpeg" style="width: 350px; height: 500px;">
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-sm-6">
                <div class="projects_item">
                    <div class="item web">
                        <a href="<?php echo base_url()?>img/product/infusion_therapy/img_2.jpeg" class="item-wrap" data-fancybox="Infusion Therapy">
                            <span style="padding:10px;"> Three Way Stopcock </span>
                            <img class="img-fluid" src="<?php echo base_url()?>img/product/infusion_therapy/img_2.jpeg" style="width: 350px; height: 500px;">
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-sm-6">
                <div class="projects_item">
                    <div class="item web">
                        <a href="<?php echo base_url()?>img/product/infusion_therapy/img_3.jpeg" class="item-wrap" data-fancybox="Infusion Therapy">
                            <span style="padding:10px;">Infusion Set Safety Double Drop Chamber</span>
                            <img class="img-fluid" src="<?php echo base_url()?>img/product/infusion_therapy/img_3.jpeg" style="width: 350px; height: 500px;">
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-sm-6">
                <div class="projects_item">
                    <div class="item web">
                        <a href="<?php echo base_url()?>img/product/infusion_therapy/img_4.jpeg" class="item-wrap" data-fancybox="Infusion Therapy">
                            <span style="padding:10px;">Safety IV Cannula With Catheter with Injection Valve & with Small Wings</span>
                            <img class="img-fluid" src="<?php echo base_url()?>img/product/infusion_therapy/img_4.jpeg" style="width: 350px; height: 500px;">
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-sm-6">
                <div class="projects_item">
                    <div class="item web">
                        <a href="<?php echo base_url()?>img/product/infusion_therapy/img_5.jpeg" class="item-wrap" data-fancybox="Infusion Therapy">
                            <span style="padding:10px;">IV Cannula Without Injection Port & Wings</span>
                            <img class="img-fluid" src="<?php echo base_url()?>img/product/infusion_therapy/img_5.jpeg" style="width: 350px; height: 500px;">
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-sm-6">
                <div class="projects_item">
                    <div class="item web">
                        <a href="<?php echo base_url()?>img/product/infusion_therapy/img_6.jpeg" class="item-wrap" data-fancybox="Infusion Therapy">
                            <span style="padding:10px;">Safety IV Cannula With Catheter with Injection Valve & with Small Wings (PLUS)</span>
                            <img class="img-fluid" src="<?php echo base_url()?>img/product/infusion_therapy/img_6.jpeg" style="width: 350px; height: 500px;">
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-sm-6">
                <div class="projects_item">
                    <div class="item web">
                        <a href="<?php echo base_url()?>img/product/infusion_therapy/img_7.jpeg" class="item-wrap" data-fancybox="Infusion Therapy">
                            <span style="padding:10px;">Safety IV Cannula With Catheter with Injection Valve & with Small Wings</span>
                            <img class="img-fluid" src="<?php echo base_url()?>img/product/infusion_therapy/img_7.jpeg" style="width: 350px; height: 500px;">
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--================End Projects Area =================-->