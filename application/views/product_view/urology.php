<!-- start banner Area -->
<section class="relative" id="about" style="background: url('<?php echo base_url()?>/img/header-bg-product.jpg') center;
  background-size: cover;">	
	<div class="overlay overlay-bg"></div>
	<div class="container">				
		<div class="row d-flex align-items-center justify-content-center">
			<div class="about-content col-lg-12">
				<h1 class="text-white">
					Our Product				
				</h1>	
				<p class="text-white link-nav"><a href="<?php echo base_url()?>">Home </a>  <span class="lnr lnr-arrow-right"></span>  <a href="<?php echo base_url()?>product"> Product</a></p>
			</div>	
		</div>
	</div>
</section>
<!-- End banner Area -->

<!--================App Feature Area =================-->
    <section class="app_feature_area " id="feature">
        <div class="container">
          <div class="row justify-content-center">
            <div class="col-md-12 text-center">
              <h1 class="font-weight-bold text-black">Learning Hub</h1>
              <p class="mb-5">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eligendi, explicabo, quasi. Magni deserunt sunt labore.</p>
            </div>
          </div>
            <div class="row app_feature_inner">
                <div class="col-md-3">
                    <div class="app_feature_item_inner">
                        <div class="app_feature_item">
                            <div class="round_icon">
                                <i class="fa fa-sign-language"></i>
                            </div>
                            <a href="#"><h4>Easy to Use</h4></a>
                            <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered.</p>
                        </div>
                        <div class="app_feature_item">
                            <div class="round_icon">
                                <i class="fa fa-diamond"></i>
                            </div>
                            <a href="#"><h4>Unique Design</h4></a>
                            <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered.</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="app_round_image">
                        <img class="round_c" src="img/round-circle.png" alt="">
                        <a href="https://demo.learning-hub.id/" target="_blank">
                        	<img class="app_mobile" src="<?php echo base_url();?>img/app-mobile.png" alt="">
                        </a>
                    </div>
                    <div class="app_feature_item app_middle">
                        <div class="round_icon">
                            <i class="fa fa-certificate"></i>
                        </div>
                        <a href="#"><h4>E-Certificate</h4></a>
                        <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration .</p>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="app_feature_item_inner left_feature">
                        <div class="app_feature_item">
                            <div class="round_icon">
                                <i class="fa fa-sort-amount-asc"></i>
                            </div>
                            <a href="#"><h4>Reduce Cost</h4></a>
                            <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered.</p>
                        </div>
                        <div class="app_feature_item">
                           <div class="round_icon">
                                <i class="fa fa-arrows-alt"></i>
                            </div>
                            <a href="#"><h4>Very Flexible</h4></a>
                            <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--================End App Feature Area =================-->

<!--================Provide Feature Area =================-->
<section class="provide_feature_area" id="feature">
    <div class="p_feature_left">
        <div class="p_f_left_content">
            <h3 class="single_title">IZORA</h3>
            <p class="mb-5">We provide features that you need !</p>
            <div class="p_left_item_inner">
                <div class="p_item">
                    <div class="media">
                        <div class="media-left">
                            <img src="img/icon/p-icon-1.png" alt="">
                        </div>
                        <div class="media-body">
                            <h4>Perfect Dashboard</h4>
                            <p>There's lot of hate out there for a text that amounts to little more than garbled words.</p>
                        </div>
                    </div>
                </div>
                <div class="p_item">
                    <div class="media">
                        <div class="media-left">
                            <img src="img/icon/p-icon-2.png" alt="">
                        </div>
                        <div class="media-body">
                            <h4>SEO Marketing</h4>
                            <p>There's lot of hate out there for a text that amounts to little more than garbled words.</p>
                        </div>
                    </div>
                </div>
                <div class="p_item">
                    <div class="media">
                        <div class="media-left">
                            <img src="img/icon/p-icon-3.png" alt="">
                        </div>
                        <div class="media-body">
                            <h4>Unique Design</h4>
                            <p>There's lot of hate out there for a text that amounts to little more than garbled words.</p>
                        </div>
                    </div>
                </div>
                <div class="p_item">
                    <div class="media">
                        <div class="media-left">
                            <img src="img/icon/p-icon-4.png" alt="">
                        </div>
                        <div class="media-body">
                            <h4>Data Analytics</h4>
                            <p>There's lot of hate out there for a text that amounts to little more than garbled words.</p>
                        </div>
                    </div>
                </div>
                <div class="p_item">
                    <div class="media">
                        <div class="media-left">
                            <img src="img/icon/p-icon-5.png" alt="">
                        </div>
                        <div class="media-body">
                            <h4>Easy Customizable</h4>
                            <p>There's lot of hate out there for a text that amounts to little more than garbled words.</p>
                        </div>
                    </div>
                </div>
                <div class="p_item">
                    <div class="media">
                        <div class="media-left">
                            <img src="img/icon/p-icon-6.png" alt="">
                        </div>
                        <div class="media-body">
                            <h4>Easy Log-in / Log-out</h4>
                            <p>There's lot of hate out there for a text that amounts to little more than garbled words.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="p_feature_right">
        <div class="p_feature_img">
            <img src="<?php echo base_url();?>img/provide-ds-img.jpg" alt="">
        </div>
    </div>
</section>
<!--================End Provide Feature Area =================-->


<!-- Start project Area -->
	<section class="project-area section-gap" id="project">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-md-8 pb-30 header-text text-center">
					<h1 class="mb-10">Screenshots our dashboard works</h1>
					<p>
						Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
					</p>
				</div>
			</div>						
			<div class="row">
				<div class="col-lg-8 col-md-8">
					<a href="img/p1.jpg" class="img-gal">
						<img class="img-fluid single-project" src="img/p1.jpg" alt="">
					</a>	
				</div>
				<div class="col-lg-4 col-md-4">
					<a href="img/p2.jpg" class="img-gal">
						<img class="img-fluid single-project" src="img/p2.jpg" alt="">
					</a>	
				</div>						
				<div class="col-lg-6 col-md-6">
					<a href="img/p3.jpg" class="img-gal">
						<img class="img-fluid single-project" src="img/p3.jpg" alt="">
					</a>	
				</div>
				<div class="col-lg-6 col-md-6">
					<a href="img/p4.jpg" class="img-gal">
						<img class="img-fluid single-project" src="img/p4.jpg" alt="">
					</a>	
				</div>		
			</div>
		</div>	
	</section>
	<!-- End project Area -->