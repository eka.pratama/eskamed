
<!-- start footer Area -->		
<footer class="footer-area section-gap">
	<div class="container">

		<div class="row">
			<?php foreach($footer as $data_footer) { ?>
				<div class="col-lg-3 col-md-6 col-sm-6">
					<div class="single-footer-widget">
						<h6><?php echo $data_footer->title; ?></h6>
						<p>
							<?php echo $data_footer->description; ?>
						</p>								
					</div>
				</div>		
			<?php } ?>	
		</div>

	</br>
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12">
				<p class="footer-text text-center">
					<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
					Copyright &copy; <script>document.write(new Date().getFullYear());</script> Eskamed Indonesia. All rights reserved
					<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
				</p>								
				</div>
			</div>
		</div>
	</div>
</footer>	
<!-- End footer Area -->

<?php
    $this->load->view('components/java_script');
?>
</body>
</html>